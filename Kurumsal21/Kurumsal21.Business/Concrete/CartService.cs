﻿using Kurumsal21.Business.Abstract;
using Kurumsal21.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurumsal21.Business.Concrete
{
    public class CartService : ICartService
    {
        public void AddToCart(Cart cart, Product product)
        {
            Cartline cartline = cart.Cartlines.FirstOrDefault(c => c.Product.ProductId == product.ProductId);
            if (cartline!=null)
            {
                cartline.Quantity++;
                return;
            }
            cart.Cartlines.Add(new Cartline { Product = product,Quantity=1 });
        }

        public List<Cartline> List(Cart cart)
        {
            return cart.Cartlines;
        }

        public void RemoveFromCart(Cart cart, int productId)
        {
            cart.Cartlines.Remove(cart.Cartlines.FirstOrDefault(c=>c.Product.ProductId==productId));
        }
    }
}
