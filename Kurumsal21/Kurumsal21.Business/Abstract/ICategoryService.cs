﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.Business.Abstract
{
    public interface ICategoryService
    {
        List<Category> GetAll();

    }
}
