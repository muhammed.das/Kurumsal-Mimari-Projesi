﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Services
{
    public interface ICartSessionService
    {
        Cart GetCart();
        void SetCart(Cart cart);
    }
}
