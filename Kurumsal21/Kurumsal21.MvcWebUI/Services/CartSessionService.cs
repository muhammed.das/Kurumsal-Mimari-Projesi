﻿using Kurumsal21.Entities.Concrete;
using Kurumsal21.MvcWebUI.ExtensionMethods;

namespace Kurumsal21.MvcWebUI.Services
{
    public class CartSessionService : ICartSessionService
    {
        private IHttpContextAccessor _httpContextAccesor;
        public CartSessionService(IHttpContextAccessor httpContextAccesor)
        {
            _httpContextAccesor= httpContextAccesor;
        }
        public Cart GetCart()
        {
           Cart cartToCheck = _httpContextAccesor.HttpContext.Session.GetObject<Cart>("cart");
            if (cartToCheck == null)
            {
                _httpContextAccesor.HttpContext.Session.SetObject( new Cart(), "cart");
                cartToCheck = _httpContextAccesor.HttpContext.Session.GetObject<Cart>("cart");
            }
            return cartToCheck;
        }

        public void SetCart(Cart cart)
        {
            _httpContextAccesor.HttpContext.Session.SetObject( cart, "cart");
        }
    }
}
