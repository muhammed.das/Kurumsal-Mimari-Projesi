﻿using Kurumsal21.Business.Abstract;
using Kurumsal21.MvcWebUI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace Kurumsal21.MvcWebUI.ViewComponents
{
    public class CategoryListViewComponent : ViewComponent
    {
        private ICategoryService _categoryService;
        public CategoryListViewComponent(ICategoryService categoryService)
        {
                    _categoryService = categoryService;
               
        }

        public ViewViewComponentResult Invoke()
        {
            var model = new CategoryListViewModel
            {
                Categories = _categoryService.GetAll(),
                CurrentCategory =Convert.ToInt32(HttpContext.Request.Query["category"])
            };
            return View(model);
        }

    }



}
