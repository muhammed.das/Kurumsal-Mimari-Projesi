﻿using Microsoft.Extensions.FileProviders;

namespace Kurumsal21.MvcWebUI.Middlewares
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseNodeModules( this IApplicationBuilder app, string root)
        {
            var path = Path.Combine(root, "node_modules");
            var provider = new PhysicalFileProvider(path);//benim için fiziksel dosya barindirma işlemini gerçekleştir.
            var options = new StaticFileOptions();

            //"/node_modules gibi bir istek gelirse
            ////dosya sunumu fileproider yapacak buda providerse denk gelmektedir
            options.RequestPath = "/node_modules";
            options.FileProvider= provider;

            app.UseStaticFiles(options);

            return app;
        }
    }
}
