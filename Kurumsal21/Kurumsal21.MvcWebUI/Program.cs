using Kurumsal21.Business.Abstract;
using Kurumsal21.Business.Concrete;
using Kurumsal21.Dal.Abstract;
using Kurumsal21.Dal.Concrete.EntityFramework;
using Kurumsal21.MvcWebUI.Entities;
using Kurumsal21.MvcWebUI.Middlewares;
using Kurumsal21.MvcWebUI.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddMvc();
builder.Services.AddScoped<IProductService, ProductManager>();
builder.Services.AddScoped<IProductDal, EfProductDal>();

builder.Services.AddScoped<ICategoryService, CategoryManager>();
builder.Services.AddScoped<ICategoryDal, EfCategoryDal>();
builder.Services.AddSingleton<ICartSessionService, CartSessionService>();
builder.Services.AddSingleton<ICartService, CartService>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

builder.Services.AddDbContext<CustomIdentityDbContext>
    (options => options.UseSqlServer("Server=(localdb)\\MSSQLLocalDB ; Database=Northwind;Trusted_Connection=true"));
builder.Services.AddIdentity<CustomIdentityUser, CustomIdentityRole>()
    .AddEntityFrameworkStores<CustomIdentityDbContext>()
    .AddDefaultTokenProviders();

builder.Services.AddSession();
//yazmamizin sebebi sessionlarin aktif hale gelmesidir//
builder.Services.AddDistributedMemoryCache();

var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    //app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    //app.UseNodeModules(app.Environment.ContentRootPath);
}

app.UseAuthentication();

app.UseSession();


app.UseHttpsRedirection();
//app.UseStaticFiles();
app.UseFileServer(); // Static dosyalari tutar  wwwrootun disindada dosyalarda gelen requestlerede cevap vermemizi saglar


app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Product}/{action=Index}/{id?}");

app.Run();
