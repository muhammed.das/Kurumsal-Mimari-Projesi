﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Models
{
    public class CategoryListViewModel
    {
        public List<Category> Categories { get; internal set; }
        public int CurrentCategory { get; internal set; }
    }
}
