﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Models
{
    public class ProductUpdateViewModel
    {
        public Product Product { get; set; }
        public List<Category> Categories { get; set; }
    }
}