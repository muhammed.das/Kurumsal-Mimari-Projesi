﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI
{
    public class CartSummaryViewModel
    {
        public Cart Cart { get; set; }
    }
}