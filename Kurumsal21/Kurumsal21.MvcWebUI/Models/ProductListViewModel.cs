﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Models
{
    public class ProductListViewModel
    {
        public List<Product> Products { get; internal set; }
        public int PageCount { get; internal set; }
        public int CurrentCategory { get; internal set; }
        public int CurrentPage { get; internal set; }
        public int PageSize { get; internal set; }
    }
}
