﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Models
{
    public class ProductAddViewModel
    {
        public  Product Product { get; set; }
        public List<Category> Categories { get; internal set; }
    }
}
