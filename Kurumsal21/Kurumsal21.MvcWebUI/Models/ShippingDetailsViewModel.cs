﻿using Kurumsal21.Entities.Concrete;

namespace Kurumsal21.MvcWebUI.Models
{
    public class ShippingDetailsViewModel
    {
        public ShippingDetails ShippingDetails { get; set; }
    }
}
