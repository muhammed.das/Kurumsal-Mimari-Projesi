﻿using Kurumsal21.Dal.Abstract;
using Kurumsal21.Entities.Concrete;
using Kurumusal21.Core.Dal.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurumsal21.Dal.Concrete.EntityFramework
{
    // BUNU yaptığımızda artık veri tabanında bütün Crud sorguları hazır durumda olacaktır.
    public class EfProductDal:EfEntityRepositoryBase<Product,NortwindContext>,IProductDal
    {

    }
}
