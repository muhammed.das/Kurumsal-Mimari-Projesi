﻿using Kurumsal21.Dal.Abstract;
using Kurumsal21.Entities.Concrete;
using Kurumusal21.Core.Dal.EntityFramework;

namespace Kurumsal21.Dal.Concrete.EntityFramework
{
    public class EfCategoryDal : EfEntityRepositoryBase<Category, NortwindContext>, ICategoryDal
    {

    }
}
