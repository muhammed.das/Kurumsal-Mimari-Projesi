﻿using Kurumsal21.Entities.Concrete;
using Kurumusal21.Core.Dal;

namespace Kurumsal21.Dal.Abstract
{
    public interface ICategoryDal : IEntityRepository<Category>
    {
      
    }
}
