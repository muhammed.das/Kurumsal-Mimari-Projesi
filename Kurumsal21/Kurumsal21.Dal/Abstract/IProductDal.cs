﻿using Kurumsal21.Entities.Concrete;
using Kurumusal21.Core.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurumsal21.Dal.Abstract
{
    public interface IProductDal:IEntityRepository<Product>
    {
        // Burayı kullanmamızın sebebi Product ile ilgili 
        //Özel operasyonları buraya yazarız sadece produtta lazım olan operasyonlar

    }
}
