﻿using Kurumusal21.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kurumusal21.Core.Dal.EntityFramework
{
    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity : IEntity, new()
        where TContext : DbContext, new()
    {

        public TEntity Get(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                return context.Set<TEntity>().SingleOrDefault(filter);
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                if (filter ==null)
                {
                    return context.Set<TEntity>().ToList();
                }
                else
                {
                    return context.Set<TEntity>().Where(filter).ToList();
                }

            }
        }

        public void Add(TEntity entity)
        {
            using (var context = new TContext())
            {
               var addedEntity = context.Entry(entity); //Enty hangi obje üzerinde çalışacağımızı belirttiğimiz bir nesne
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
            
        }

        public void Delete(TEntity entity)
        {
            using (var context = new TContext())
            {
                var DeletedEntity = context.Entry(entity); //Enty hangi obje üzerinde çalışacağımızı belirttiğimiz bir nesne
                DeletedEntity.State = EntityState.Deleted;
                context.SaveChanges();
            }
        }


        public void Update(TEntity entity)
        {
            using (var context = new TContext())
            {
                var updatedEntity = context.Entry(entity); //Enty hangi obje üzerinde çalışacağımızı belirttiğimiz bir nesne
                updatedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
