﻿namespace Kurumsal21.Entities.Concrete
{
    public class Cart
    {
        public Cart()
        {
            Cartlines = new List<Cartline>();
        }
        public List<Cartline> Cartlines { get; set; }

        public decimal Total
        {
            get { return Cartlines.Sum(c => c.Product.UnitPrice*c.Quantity); }
        }
    }
}
